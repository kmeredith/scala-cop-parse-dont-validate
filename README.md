## Customer ID:

A well-formed Customer ID consists of 2 digits for their institution, 
and then 6 digits for their individual ID. 

11 123456
22 123456 

The Primary Key = (institution_id, individual_id)