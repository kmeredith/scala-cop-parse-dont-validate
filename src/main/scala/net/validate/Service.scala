package net.validate

object Service {

  def processInput(input: String): Either[String, Int] = {
    for {
      _ <- validateCustomerId(input)
    } yield institutionLookup(input.take(2))
  }

  private def validateCustomerId(input: String): Either[String, Unit] = {
    val cond: Boolean =
      input.length == 8 && input.forall(_.isDigit)

    if (cond) Right(()) else Left("Invalid customer id")
  }

  private def institutionLookup(institutionId: String): Int = ???

}
