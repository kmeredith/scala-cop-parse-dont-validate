package net.validate

object Api {

  final case class Result(quantity: Int)

  def processInput(input: String): Either[String, Result] = {
    for {
      _ <- validateCustomerId(input)
      result <- Service.processInput(input)
    } yield Result(result)
  }

  private def validateCustomerId(input: String): Either[String, Unit] = {
    val cond: Boolean =
      input.length == 8 && input.forall(_.isDigit)
    if (cond) Right(()) else Left("Invalid customer id")
  }


}
