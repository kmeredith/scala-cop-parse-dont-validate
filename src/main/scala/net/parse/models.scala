package net.parse

final class CustomerId private (val institution: String, val individual: String) {
  override def toString: String = s"$institution-$individual"
}
object CustomerId {
  
  // Start + 2 digits + 6 digits + end
  private val Pattern = "^([0-9]{2})([0-9]{6})$".r

  def fromString(input: String): Option[CustomerId] = input match {
    case Pattern(institution, individual) => Some { new CustomerId(institution, individual) }
    case _ => None
  }
}
