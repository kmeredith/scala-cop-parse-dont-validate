package net.parse

object Api {

  final case class Result(quantity: Int)

  def processInput(input: String): Either[String, Result] = {
    for {
      customer <- CustomerId.fromString(input).toRight("InvalidCustomerId")
      result <- Service.processInput(customer)
    } yield Result(result)
  }
}
